import fastf1 as ff1

class session():

    def load_session(year, round_, session_type):
        """Returns the lap data for a F1 session"""
        session_loaded = ff1.get_session(year, round_, session_type) # Get Session Data
        laps = session_loaded.load_laps(with_telemetry=True)
        return (session_loaded.weekend.name, session_loaded.name, laps)

    def get_telemetry_data_for_compare_laps(drivers, session_lap_data, compare_lap):
        """Pulls the telemetry data for the chosen lap(s) & driver(s) and returns the DataFrames in a list"""
        telemetry = []
        for i, driver in enumerate(drivers):
            if compare_lap == 'fastest':
                lap = session_lap_data.pick_driver(driver).pick_fastest()
            elif type(compare_lap) == int:
                lap = session_lap_data.pick_driver(driver).loc[session_lap_data.LapNumber == compare_lap]
            elif type(compare_lap) == list:
                lap = session_lap_data.pick_driver(driver).loc[session_lap_data.LapNumber == compare_lap[i]]
            telemetry_data = lap.get_telemetry().add_distance()
            telemetry.append(telemetry_data)
        
        return telemetry