import fastf1 as ff1
from pygments import highlight
from f1analytics.Session import session
from f1analytics.Visualizer import vizualizer
from f1analytics import CalculationHelpers
import pandas as pd

class TyreCompare():

    def __init__(self, year=2021, round_=21, session_type='R', stint=1, filter_slow_laps=True, ylims=[88, 95], drivers_compare_no='all', compounds=['SOFT', 'MEDIUM', 'HARD'], highlight_drivers=None, background_picture_path=None, save_figure_path=None) -> None:
        """
        A method to show the tyre performance on a specific stint

        Input Parameters:
            year: int
            round_: int
            session_type: str ('R', 'Q', 'FP1', 'FP2', 'FP3')
            stint: int
            filter_slow_laps: if True, all SafetyCar-Laps, PitStop Laps, RedFlag Laps will be excluded from the graph
            ylims: [y_low, y_high]
            drivers_compare_no: list of drivers to include (default: 'all')
            compounds: [tyres to analyse] (default: ['SOFT', 'MEDIUM', 'HARD'])
            highlight_drivers: [list of drivers to highlight in the graph] (default: None)
            background_picture_path: str or None (path to a bg-picture (size 12:4))
            save_figure_path: str or None (path to a folder to save the output figure in)
        """
        self.session_type = session_type
        self.weekend_title, self.session_name, self.session_lap_data = session.load_session(year, round_, self.session_type)
        self.session_lap_data, self.racing_median = CalculationHelpers.enhance_session_lap_data(self.session_lap_data)

        self.filtered_data = CalculationHelpers.filter_data_on_stint(self.session_lap_data, compounds, drivers_compare_no, stint)

        if filter_slow_laps:
            self.filtered_data = CalculationHelpers.filter_slow_laps(self.filtered_data)

        vizualizer.visualize_tyre_performance(
            data=self.filtered_data, 
            stint=stint, 
            overall_median_lap_time=self.racing_median, 
            weekend_title=self.weekend_title + " " + self.session_name, 
            ylims=ylims, 
            highlight_drivers=highlight_drivers, 
            background_picture_path=background_picture_path, 
            save_figure_path=save_figure_path)
