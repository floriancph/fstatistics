from tkinter import font
from turtle import color
import matplotlib.pyplot as plt
import matplotlib.image as image
import seaborn as sns
import pandas as pd
from f1analytics import statics, CalculationHelpers

class vizualizer():

    def calculate_telemetry_data_with_boundaries(boundaries, telemetry_data):
        start = boundaries[0]
        stop = boundaries[1]
        telemetry_data_new = []
        for data in telemetry_data:
            sub_data = data.loc[(data.Distance < stop) & (data.Distance > start)]
            telemetry_data_new.append(sub_data)
        return telemetry_data_new

    def visualize_driver_comparison(telemetry_data, drivers, weekend_title, lap_no, title_extension, attributes, total_amount_of_laps, boundaries=None, background_picture_path=None, save_figure_path=None):
        if len(drivers) > 10:
            print("Maximum number of drivers is 10")
            return None

        if boundaries is not None:
            telemetry_data = vizualizer.calculate_telemetry_data_with_boundaries(boundaries=boundaries, telemetry_data=telemetry_data)

        if lap_no == 'fastest':
            title = f'{weekend_title} - fastest Lap'
        else:
            title = f'{weekend_title} - Lap {lap_no}'
            if total_amount_of_laps is not None:
                title += f"/{total_amount_of_laps}"

        if title_extension != "":
            title = title + '- {title_extension}'

        linestyles = ['solid', 'dashed', 'solid', 'dashed', 'solid', 'dashed', 'solid', 'dashed', 'solid', 'dashed']

        sns.set(style="ticks", context="talk")
        plt.style.use("dark_background")

        graphs = len(attributes)
        graph_height = graphs * 4

        plt.figure(figsize=(12, graph_height))

        for i, attribute in enumerate(attributes):
            plt.subplot(graphs, 1, i+1)

            overall_minimum = None
            overall_maximum = None

            for i, driver in enumerate(telemetry_data):
                sns.lineplot(x='Distance', y=attribute, data=driver, label=statics.driver_names[drivers[i]], color=statics.team_colors[statics.driver_teams[drivers[i]]], linewidth=2, linestyle=linestyles[i])
                if i == 0:
                    overall_minimum = driver[attribute].min()
                    overall_maximum = driver[attribute].max()
                
                if driver[attribute].min() < overall_minimum:
                    overall_minimum = driver[attribute].min()

                if driver[attribute].max() > overall_maximum:
                    overall_maximum = driver[attribute].max()
            
            if background_picture_path is not None:
                im = image.imread(background_picture_path)
                plt.imshow(im, aspect='auto', extent=(driver.Distance.min(), driver.Distance.max(), overall_minimum-(overall_maximum*0.1), overall_maximum*1.1), zorder=-1, alpha=0.3)
                
            plt.title(f"{title} - {attribute}")

        plt.tight_layout()
        if save_figure_path is not None:
            plt.savefig(save_figure_path)

        plt.show()

    def visualize_driver_comparison_track(telemetry_data, drivers, weekend_title, lap_no, title_extension, total_amount_of_laps, attributes, text_box_position, tyre_ages=None, compounds=None, lap_times=None, boundaries=None, background_picture_path=None, save_figure_path=None):

        if boundaries is not None:
            telemetry_data = vizualizer.calculate_telemetry_data_with_boundaries(boundaries=boundaries, telemetry_data=telemetry_data)

        if lap_no == 'fastest':
            title = f'{weekend_title} - fastest Lap'
        else:
            title = f'{weekend_title} - Lap {lap_no}'
            if total_amount_of_laps is not None:
                title += f"/{total_amount_of_laps}"

        if title_extension != "":
            title = title + '- {title_extension}'

        sns.set(style="ticks", context="talk")
        plt.style.use("dark_background")

        for i, driver_no in enumerate(drivers):
            plt.figure(figsize=(14,14))
            sns.scatterplot(x='X', y='Y', data=telemetry_data[i], marker="s", s=70, hue=attributes[0])
            
            plt.axis('off')
            plt.title(f"{title} - {attributes[0]} {statics.driver_names[driver_no]}")

            x_min = telemetry_data[i].X.min()
            x_max = telemetry_data[i].X.max()
            x_diff = x_max - x_min
            x_padding = x_diff*0.1

            y_min = telemetry_data[i].Y.min()
            y_max = telemetry_data[i].Y.max()
            y_diff = y_max - y_min
            y_padding = y_diff*0.1

            # Info Box
            if text_box_position == 'top_left':
                x, y = x_min, y_max
                horizontalalignment = 'left'
                verticalalignment = 'top'
                plt.legend(loc='upper right')
            elif text_box_position == 'top_right':
                x, y = x_max, y_max
                horizontalalignment = 'right'
                verticalalignment = 'top'
                plt.legend(loc='lower right')
            elif text_box_position == 'center':
                x, y = x_min+(x_diff/2), y_min+(y_diff/2)
                horizontalalignment = 'center'
                verticalalignment = 'center'
                plt.legend(loc='upper right')

            text_box_text = f"""Top-Speed: {telemetry_data[i].Speed.max()} km/h\nAverage Speed: {round(telemetry_data[i].Speed.mean(), 2)} km/h"""

            if lap_times[i] is not None:
                text_box_text += f"\nLap Time: {lap_times[i]} sec."
            
            if compounds[i] is not None:
                text_box_text += f"\nTyre: {compounds[i]}"

            if tyre_ages[i] is not None:
                if int(tyre_ages[i]) == 1:
                    text_box_text += f"\nTyre Age: {int(tyre_ages[i])} Lap"
                else:
                    text_box_text += f"\nTyre Age: {int(tyre_ages[i])} Laps"
            
            # plot info box
            plt.text(x, y, text_box_text, horizontalalignment=horizontalalignment, verticalalignment=verticalalignment, bbox=dict(boxstyle='round,pad=0.7', facecolor='green', alpha=0.4, linewidth=4))

            # plot maximum speed of the lap
            max_speed_x = telemetry_data[i].loc[telemetry_data[i].Speed == telemetry_data[i].Speed.max()].X.to_list()[0]
            max_speed_y = telemetry_data[i].loc[telemetry_data[i].Speed == telemetry_data[i].Speed.max()].Y.to_list()[0]
            plt.text(max_speed_x, max_speed_y, 'Fastest Speed', horizontalalignment='center', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.2', facecolor='red', alpha=0.7, linewidth=2))

            # plot minimum speed of the lap
            min_speed_x = telemetry_data[i].loc[telemetry_data[i].Speed == telemetry_data[i].Speed.min()].X.to_list()[0]
            min_speed_y = telemetry_data[i].loc[telemetry_data[i].Speed == telemetry_data[i].Speed.min()].Y.to_list()[0]
            plt.text(min_speed_x, min_speed_y, 'Slowest Speed', horizontalalignment='center', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.2', facecolor='orange', alpha=0.7, linewidth=2))

            # plot start-finish line
            plt.text(telemetry_data[i].iloc[0].X, telemetry_data[i].iloc[0].Y, 'Start/Finish', horizontalalignment='center', verticalalignment='center', color='black', bbox=dict(boxstyle='round,pad=0.2', facecolor='white', alpha=0.7, linewidth=2))

            if background_picture_path is not None:
                im = image.imread(background_picture_path)
                plt.imshow(im, aspect='auto', extent=(x_min-x_padding, x_max+x_padding, y_min-y_padding, y_max+y_padding), zorder=-1, alpha=0.5)

            if save_figure_path is not None:
                plt.savefig(save_figure_path)
            
            plt.show()

    def visualize_driver_comparison_laptimes_boxplot(drivers, lap_times, overall_median_lap_time, weekend_title, title_extension, attributes, boundary_lap_time, background_picture_path=None, save_figure_path=None):
        sns.set(style="ticks", context="talk")
        plt.style.use("bmh")
        plt.figure(figsize=(14,14), facecolor='black')
        plt.tick_params(axis='both', labelcolor='white', color='white')

        palette = {}
        for driver in drivers:
            palette[statics.driver_names[driver]] = statics.team_colors[statics.driver_teams[driver]]

        sns.boxplot(y=attributes[0], data=lap_times, x='DriverName', showfliers=True, palette=palette, linewidth=5)
        plt.hlines(overall_median_lap_time, -0.5, len(drivers)-0.5, colors="white", linestyles='dotted', linewidth=5)
        plt.text(-0.45, overall_median_lap_time, 'Overall Median', color="white", horizontalalignment='left', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.4', facecolor='black', alpha=0.7, linewidth=2))

        used_tyres = list(lap_times.Compound.unique())
        colors = []
        for tyre in used_tyres:
            colors.append(statics.tyre_colors[tyre])

        sns.swarmplot(x='DriverName', y=attributes[0], data=lap_times, hue="Compound", size=8, palette=sns.color_palette(colors))

        if background_picture_path is not None:
            im = image.imread(background_picture_path)
            plt.imshow(im, aspect='auto', extent=(-0.5, len(drivers)-0.5, boundary_lap_time[0], boundary_lap_time[1]), zorder=-1, alpha=0.6)

        title = f'{weekend_title}'

        if title_extension != "":
            title = title + '- {title_extension}'

        plt.title(title, color='white')

        if save_figure_path is not None:
            plt.savefig(save_figure_path)

        plt.xlabel("")
        plt.ylabel(str(attributes[0]), color='white')

        plt.show()

    def visualize_driver_comparison_laptimes_lineplot(drivers, lap_times, pit_in_laps, overall_median_lap_time, weekend_title, title_extension, attributes, boundary_lap_time, background_picture_path=None, save_figure_path=None):
        sns.set(style="ticks", context="talk")
        plt.style.use("dark_background")

        plt.figure(figsize=(14,14))

        for n, driver in enumerate(drivers):
            sns.lineplot(x='LapNumber', y=attributes[0], data=lap_times.pick_driver(driver).loc[lap_times.Box == 0], linewidth=4, color=statics.team_colors[statics.driver_teams[driver]], alpha=0.7)
            driver_df = pit_in_laps.pick_driver(driver).copy().reset_index()
            for i in range(0, len(driver_df)):
                lap_number = driver_df.iloc[i].LapNumber
                if lap_number != 1:
                    y_value = lap_times.pick_driver(driver).loc[lap_times.LapNumber == lap_number-1][attributes[0]]
                    plt.text(lap_number, y_value, "PitStop", horizontalalignment='left', verticalalignment='center', bbox=dict(boxstyle='square,pad=0.1', facecolor=statics.team_colors[statics.driver_teams[driver]], alpha=0.8, linewidth=2))
        
            plt.text(1, lap_times.pick_driver(driver).loc[lap_times.LapNumber == 2][attributes[0]], statics.driver_names[driver], horizontalalignment='left', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.3', facecolor=statics.team_colors[statics.driver_teams[driver]], alpha=0.6, linewidth=2))

        used_tyres = list(lap_times.Compound.unique())
        colors = []
        for tyre in used_tyres:
            colors.append(statics.tyre_colors[tyre])

        sns.scatterplot(x='LapNumber', y=attributes[0], data=lap_times.loc[lap_times.Box == 0], hue="Compound", s=200, palette=sns.color_palette(colors))

        plt.hlines(overall_median_lap_time, 0, lap_times.LapNumber.max(), colors="white", linestyles='dotted', linewidth=5)
        plt.text(lap_times.LapNumber.max() -3, overall_median_lap_time, 'Overall Median', color="white", horizontalalignment='right', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.4', facecolor='black', alpha=0.7, linewidth=2))

        if background_picture_path is not None:
            im = image.imread(background_picture_path)
            plt.imshow(im, aspect='auto', extent=(0, lap_times.LapNumber.max(), boundary_lap_time[0], boundary_lap_time[1]), zorder=-1, alpha=0.6)

        plt.ylim(boundary_lap_time[0], boundary_lap_time[1])

        title = f'{weekend_title}'

        if title_extension != "":
            title = title + '- {title_extension}'

        plt.title(title)

        if save_figure_path is not None:
            plt.savefig(save_figure_path)

        plt.show()

    def visualize_tyre_performance(data, stint, overall_median_lap_time, weekend_title, highlight_drivers, ylims=[88, 95], background_picture_path=None, save_figure_path=None):
        sns.set(style="ticks", context="talk")
        plt.style.use("dark_background")
        plt.figure(figsize=(14,14))

        used_tyres = list(data.Compound.unique())
        colors = []
        for tyre in used_tyres:
            colors.append(statics.tyre_colors[tyre])
        
        alpha_value = 1.0
        if highlight_drivers is not None:
            alpha_value = 0.5

        sns.scatterplot(x='TyreLife', y='LapTimeSec', data=data, hue="Compound", s=100, palette=sns.color_palette(colors), alpha=alpha_value)
        plt.hlines(overall_median_lap_time, 0, data.TyreLife.max(), colors="white", linestyles='dotted', linewidth=5)
        plt.text(data.TyreLife.max()-2, overall_median_lap_time, 'Overall Median', color="white", horizontalalignment='right', verticalalignment='center', bbox=dict(boxstyle='round,pad=0.4', facecolor='black', alpha=0.7, linewidth=2))

        if highlight_drivers is not None:
            for driver in highlight_drivers:
                sns.scatterplot(x='TyreLife', y='LapTimeSec', data=data.pick_driver(driver), s=350, color=statics.team_colors[statics.driver_teams[driver]])

        if background_picture_path is not None:
            im = image.imread(background_picture_path)
            plt.imshow(im, aspect='auto', extent=(data.TyreLife.min(), data.TyreLife.max(), ylims[0], ylims[1]), zorder=-1, alpha=0.5)

        title = f'{weekend_title} - Analyzing Stint #{stint}'
        if highlight_drivers is not None:
            title += '\nHighlighting'
            length = len(highlight_drivers)
            i = 0
            for i, driver in enumerate(highlight_drivers):
                title += f' {statics.driver_names[driver]} '
                if i < length-1:
                    title += '&'

        plt.title(title)
        plt.ylim(ylims[0], ylims[1])

        if save_figure_path is not None:
            plt.savefig(save_figure_path)
            
        plt.show()