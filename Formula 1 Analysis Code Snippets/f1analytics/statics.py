driver_names = {
    1: "Verstappen",
    3: "Ricciardo",
    4: "Norris",
    5: "Vettel",
    6: "Latifi",
    9: "Mazepin",
    10: "Gasly",
    11: "Perez",
    14: "Alonso",
    16: "Leclerc",
    18: "Stroll",
    22: "Tsunoda",
    23: "Albon",
    24: "Zhou",
    31: "Ocon",
    33: "Verstappen",
    44: "Hamilton",
    47: "Schumacher",
    55: "Sainz",
    63: "Russell",
    77: "Bottas",
    99: "Giovinazzi"
}

driver_teams = {
    1: "Red Bull",
    3: "McLaren",
    4: "McLaren",
    5: "Aston Martin",
    6: "Williams",
    9: "Haas",
    10: "AlphaTauri",
    11: "Red Bull",
    14: "Alpine",
    16: "Ferrari",
    18: "Aston Martin",
    22: "AlphaTauri",
    23: "Williams",
    24: "Alfa Romeo",
    31: "Alpine",
    33: "Red Bull",
    44: "Mercedes",
    47: "Haas",
    55: "Ferrari",
    63: "Mercedes",
    77: "Alfa Romeo",
}

team_colors = {
    "Mercedes":     "#00D2BE",
    "Ferrari":	    "#DC0000",
    "Red Bull":     "#0600EF",
    "Alpine":	    "#0090FF",
    "Haas":	        "#FFFFFF",
    "Aston Martin":	"#006F62",
    "AlphaTauri":	"#2B4562",
    "McLaren":      "#FF8700",
    "Alfa Romeo":   "#900000",
    "Williams":     "#005AFF",
}

tyre_colors = {
    "HARD":         "#FFFFFF",
    "MEDIUM":       "#E3CB10",
    "SOFT":         "#FF4639",
    "INTERMEDIATE": "#76E070",
    "WET":          "#518FC5"
}