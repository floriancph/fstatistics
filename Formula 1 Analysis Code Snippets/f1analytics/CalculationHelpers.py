from statistics import median
import fastf1 as ff1
import pandas as pd
import numpy as np
from f1analytics import statics

def check_lap_time_null(lap_time):
    if pd.isnull(lap_time):
        return None
    else:
        return lap_time.total_seconds()

def get_lap_times_for_compare_laps(drivers, session_lap_data, compare_lap) -> list:
    """Calculates the lap-times in seconds for specific drivers and laps. If laptime = NaT, function puts None"""
    lap_times = []
    for i, driver in enumerate(drivers):
        driver_laps = session_lap_data.pick_driver(driver)
        if compare_lap == 'fastest':
            lap_time = driver_laps.pick_fastest().LapTime
        elif type(compare_lap) == int:
            lap_time = driver_laps.loc[driver_laps.LapNumber == compare_lap].LapTime.item()
        elif type(compare_lap) == list:
            lap_time = driver_laps.loc[driver_laps.LapNumber == compare_lap[i]].LapTime.item()
        lap_times.append(check_lap_time_null(lap_time))
    
    return lap_times

def convert_all_laptimes_into_seconds(session_lap_data):
        """Returns laptimes in seconds if not NaT"""
        return session_lap_data.apply(lambda r: check_lap_time_null(r.LapTime), axis=1)

def get_compounds_for_compare_laps(drivers, session_lap_data, compare_lap):
    """Returns the used compounds by the drivers for the chosen lap in a list"""
    compounds = []
    for i, driver in enumerate(drivers):
        driver_laps = session_lap_data.pick_driver(driver)
        if compare_lap == 'fastest':
            compound = driver_laps.pick_fastest().Compound
        elif type(compare_lap) == int:
            compound = driver_laps.loc[driver_laps.LapNumber == compare_lap].Compound.item()
        elif type(compare_lap) == list:
            compound = driver_laps.loc[driver_laps.LapNumber == compare_lap[i]].Compound.item()
        compounds.append(compound)
    return compounds

def get_tyre_ages_for_compare_laps(drivers, session_lap_data, compare_lap):
    """Returns the tyre ages for the chosen lap in a list"""
    tyre_ages = []
    for i, driver in enumerate(drivers):
        driver_laps = session_lap_data.pick_driver(driver)
        if compare_lap == 'fastest':
            tyre_age = driver_laps.pick_fastest().TyreLife
        elif type(compare_lap) == int:
            tyre_age = driver_laps.loc[driver_laps.LapNumber == compare_lap].TyreLife.item()
        elif type(compare_lap) == list:
            tyre_age = driver_laps.loc[driver_laps.LapNumber == compare_lap[i]].TyreLife.item()
        tyre_ages.append(tyre_age)
    return tyre_ages

def check_pit_laps(PitIn, PitOut):
    """Returns 1 for laps where the driver either enters or leaves the pits"""
    if (not pd.isnull(PitIn)) or (not pd.isnull(PitOut)):
        return 1
    else:
        return 0

def check_pit_in_laps(PitIn):
    """Returns 1 for laps where the driver only enters the pits"""
    if (not pd.isnull(PitIn)):
        return 1
    else:
        return 0

def check_safety_car_laps(TrackStatus):
    """Returns 1 for laps where Safety Car is out"""
    if int(TrackStatus) == 4:
        return 1
    else:
        return 0

def check_virtual_safety_car_laps(TrackStatus):
    """Returns 1 for laps where Virtual Safety Car was deployed"""
    if (int(TrackStatus) == 6) or (int(TrackStatus) == 7):
        return 1
    else:
        return 0

def check_red_flag_laps(TrackStatus):
    """Returns 1 for laps where Virtual Safety Car was deployed"""
    if (int(TrackStatus) == 5):
        return 1
    else:
        return 0

def calculate_median(session_lap_data):
    """returns Median for Race-Laps, excluding PitStop-Laps, SafetyCar Laps, VSC Laps and Red Flag Laps"""
    return session_lap_data.loc[(session_lap_data.Box == 0) & \
        (session_lap_data.SafetyCar == 0) & \
        (session_lap_data.VirtualSafetyCar == 0) & \
        (session_lap_data.RedFlag == 0)].LapTimeSec.median()

def enhance_session_lap_data(session_lap_data):
    """
    Enhances the session_lap_data DataFrame with additional columns such as
        LapTimeSec: Laptime converted into seconds
        Box: is '1' if driver went into the pits or came out of the pits during this lap
        PitIn: is '1' Driver went into the Pits
        SafetyCar: Safety Car was out in this lap, if value=1

    Also calculates the racing_median: Which means, that only Laps without SC and PitStops will be taken into consideration
    Returns a Tuple of (session_lap_data, racing_median)
    """
    session_lap_data['LapTimeSec'] = convert_all_laptimes_into_seconds(session_lap_data)
    session_lap_data['Box'] = session_lap_data.apply(lambda r: check_pit_laps(r['PitInTime'], r['PitOutTime']), axis=1)
    session_lap_data['PitIn'] = session_lap_data.apply(lambda r: check_pit_in_laps(r['PitInTime']), axis=1)
    session_lap_data['SafetyCar'] = session_lap_data.apply(lambda r: check_safety_car_laps(r['TrackStatus']), axis=1)
    session_lap_data['VirtualSafetyCar'] = session_lap_data.apply(lambda r: check_virtual_safety_car_laps(r['TrackStatus']), axis=1)
    session_lap_data['RedFlag'] = session_lap_data.apply(lambda r: check_red_flag_laps(r['TrackStatus']), axis=1)
    racing_median = calculate_median(session_lap_data)
    return (session_lap_data, racing_median)
    
def filter_slow_laps(session_lap_data):
    """Return only laps, which are not under safety car, VSC, red flag or pitstop"""
    return session_lap_data.loc[(session_lap_data.Box == 0) & \
        (session_lap_data.SafetyCar == 0) & \
        (session_lap_data.VirtualSafetyCar == 0) & \
        (session_lap_data.RedFlag == 0)]

def get_pit_in_laps(session_lap_data):
    return session_lap_data.loc[(session_lap_data.PitIn == 1)]

def filter_data_on_stint(session_lap_data, compounds, drivers, stint):
    """
    Returns a DataFrame with chosen tyres (compounds) and chosen drivers for a specific stint
    """
    filtered_data = session_lap_data.copy()
    filtered_data.drop(filtered_data[filtered_data.Stint != stint].index, inplace=True)

    if drivers != 'all':
        filtered_data = filtered_data.pick_drivers(drivers)

    final_compound_df = pd.DataFrame()
    for compound in compounds:
        final_compound_df = final_compound_df.append(filtered_data.pick_tyre(compound))

    return final_compound_df