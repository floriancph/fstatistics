from lib2to3.pgen2 import driver
import fastf1 as ff1
from matplotlib.pyplot import axis
from f1analytics.Session import session
from f1analytics.Visualizer import vizualizer
from f1analytics import statics, CalculationHelpers
import pandas as pd

class DriverCompareLap():

    def __init__(self, year=2021, round_=21, session_type='R', drivers_compare_no=[5, 44, 77], compare_lap='fastest', title_extension="", boundaries=None, attributes=['Speed', 'Throttle', 'Brake', 'DistanceToDriverAhead'], output_graph='telemetry', background_picture_path=None, save_figure_path=None, text_box_position='top_left') -> None:
        """
        Compares Drivers and returns a plot using telemetry data for chosen lap(s)

        Input Parameters:
            year: int
            round_: int
            session_type: str ('R', 'Q', 'FP1', 'FP2', 'FP3')
            drivers_compare_no: list (Driver Numbers to compare)
            compare_lap: str, int or list (e.g. 'fastest', lap number = 6 or lap numbers in a list for each driver i.e. [5, 8, 9])
            title_extension: str (Title Extension for the Plot Title)
            boundaries: list ([start distance (int), end distance (int)])
            attributes: list of attribute to compare ([str, str, ...]) - for the track-view only one attribute possibles
            output_graph: str or None ('telemetry', 'track', 'only_data')
            background_picture_path: str or None (path to a bg-picture (size 12:4))
            save_figure_path: str or None (path to a folder to save the output figure in)
            text_box_position: str (only for output_graph='track', default is 'top_left', others: 'top_right', 'center')

        Object Attributes to access:
            drivers: list of driver numbers
            compare_lap: laps to compare
            session_type: 'R', 'Q', 'FP1', 'FP2', 'FP3'
            weekend_title: str of the weekend name
            session_lap_data: DataFrame of all lap data from the session
            lap_times: list of laptimes (in seconds) for drivers
            compounds: list of used compounds per drivers
            tyre_ages: list of tyre ages
            lap_telemetry_data: DataFrame of the telementry data for the compare_lap
        """
        self.drivers = drivers_compare_no
        self.compare_lap = compare_lap
        self.session_type = session_type
        self.weekend_title, self.session_name, self.session_lap_data = session.load_session(year, round_, self.session_type)
        self.session_lap_data, self.racing_median = CalculationHelpers.enhance_session_lap_data(self.session_lap_data)

        if session_type == 'R':
            self.total_amount_of_laps = self.session_lap_data.LapNumber.max() # if session = race, then calculate the Maximum Lap Numbers 
        else:
            self.total_amount_of_laps = None

        self.lap_times = CalculationHelpers.get_lap_times_for_compare_laps(drivers=self.drivers, session_lap_data=self.session_lap_data, compare_lap=self.compare_lap)
        self.compounds = CalculationHelpers.get_compounds_for_compare_laps(drivers=self.drivers, session_lap_data=self.session_lap_data, compare_lap=self.compare_lap)
        self.tyre_ages = CalculationHelpers.get_tyre_ages_for_compare_laps(drivers=self.drivers, session_lap_data=self.session_lap_data, compare_lap=self.compare_lap)

        self.lap_telemetry_data = session.get_telemetry_data_for_compare_laps(drivers=self.drivers, session_lap_data=self.session_lap_data, compare_lap=self.compare_lap)

        # initiate plotting
        if output_graph == 'telemetry':
            vizualizer.visualize_driver_comparison(
                telemetry_data=self.lap_telemetry_data, 
                drivers=self.drivers, 
                total_amount_of_laps=self.total_amount_of_laps, 
                weekend_title=self.weekend_title + " " + self.session_name, 
                lap_no=compare_lap, 
                title_extension=title_extension, 
                attributes=attributes, 
                boundaries=boundaries, 
                background_picture_path=background_picture_path, 
                save_figure_path=save_figure_path)
        elif output_graph == 'track':
            vizualizer.visualize_driver_comparison_track(
                telemetry_data=self.lap_telemetry_data, 
                lap_times=self.lap_times, 
                total_amount_of_laps=self.total_amount_of_laps, 
                compounds=self.compounds, 
                tyre_ages=self.tyre_ages, 
                drivers=self.drivers, 
                weekend_title=self.weekend_title + " " + self.session_name, 
                lap_no=compare_lap, 
                title_extension=title_extension, 
                attributes=attributes, 
                boundaries=boundaries, 
                background_picture_path=background_picture_path, 
                save_figure_path=save_figure_path, 
                text_box_position=text_box_position)
        elif output_graph == 'only_data':
            print("Object created, access data via object.session_lap_data and object.lap_telemetry_data")
        else:
            print("Object created, access data via object.session_lap_data and object.lap_telemetry_data")

class DriverCompareLapStatistics():

    def __init__(self, year=2021, round_=21, session_type='R', drivers_compare_no='all', title_extension="", attributes=['LapTimeSec'], output_graph='boxplot', boundary_lap_time=[85, 110], filter_slow_laps=True, background_picture_path=None, save_figure_path=None) -> None:
        """
        Compares Drivers and returns a plot using statistical lap data for chosen lap(s)

        Input Parameters:
            year: int
            round_: int
            session_type: str ('R', 'Q', 'FP1', 'FP2', 'FP3')
            drivers_compare_no: list (Driver Numbers to compare in a list or 'all')
            title_extension: str (Title Extension for the Plot Title)
            attributes: y-Axis attribute to compare (default: ['LapTimeSec'])
            output_graph: str or None ('boxplot', 'lineplot', 'only_data')
            boundary_lap_time: y-axis-limitations (default: [85, 110])
            background_picture_path: str or None (path to a bg-picture (size 14:14))
            save_figure_path: str or None (path to a folder to save the output figure in)
        """
        
        self.year = year
        self.round_ = round_
        self.session_type= session_type
        self.weekend_title, self.session_name, self.session_lap_data = session.load_session(year, round_, self.session_type)
        self.session_lap_data, self.racing_median = CalculationHelpers.enhance_session_lap_data(self.session_lap_data)
        self.pit_in_laps = CalculationHelpers.get_pit_in_laps(self.session_lap_data)
        self.all_session_lap_data = self.session_lap_data.copy()

        if type(drivers_compare_no) == list:
            self.session_lap_data = self.session_lap_data.pick_drivers(drivers_compare_no)
        
        self.session_lap_data['DriverName'] = self.session_lap_data.apply(lambda r: statics.driver_names[int(r.DriverNumber)], axis=1)

        if filter_slow_laps:
            self.session_lap_data = CalculationHelpers.filter_slow_laps(self.session_lap_data)

        # initiate plotting
        if output_graph == 'boxplot':
            vizualizer.visualize_driver_comparison_laptimes_boxplot(
                drivers=drivers_compare_no, 
                lap_times=self.session_lap_data, 
                overall_median_lap_time=self.racing_median,
                weekend_title=self.weekend_title + " " + self.session_name, 
                title_extension=title_extension, 
                attributes=attributes, 
                boundary_lap_time=boundary_lap_time,
                background_picture_path=background_picture_path, 
                save_figure_path=save_figure_path)
        elif output_graph == 'lineplot':
            vizualizer.visualize_driver_comparison_laptimes_lineplot(
                drivers=drivers_compare_no, 
                lap_times=self.session_lap_data, 
                pit_in_laps=self.pit_in_laps,
                overall_median_lap_time=self.racing_median,
                weekend_title=self.weekend_title + " " + self.session_name, 
                title_extension=title_extension, 
                attributes=attributes, 
                boundary_lap_time=boundary_lap_time,
                background_picture_path=background_picture_path, 
                save_figure_path=save_figure_path)
        elif output_graph == 'only_data':
            print("Object created, access data via object.session_lap_data and object.all_session_lap_data")
        else:
            print("Object created, access data via object.session_lap_data and object.all_session_lap_data")