import numpy as np
import pickle as pkl
from sklearn import preprocessing

scaler = pkl.load(open('2021_heart_disease/scaler.sav', 'rb'))
clm = pkl.load(open('2021_heart_disease/final_model.p', 'rb'))

while True:
    # Ask user:
    age = input("Age: ")
    sex = input("Sex (0, 1): ")
    cp = input("Chest Pain Type (0-3): ")
    trestbps = input("Resting Blood Pressure: ")
    chol = input("serum cholestoral in mg/dl: ")
    fbs = input("Fasting blood sugar; 120 mg/dl (1 = true; 0 = false): ")
    restecg = input("Resting Electrocardiographic Results (0, 1, 2): ")
    thalach = input("Maximum heart rate achieved: ")
    exang = input("Exercise induced angina (1 = yes; 0 = no): ")
    oldpeak = input("ST depression induced by exercise relative to rest: ")
    slope = input("the slope of the peak exercise ST segment: ")
    ca = input("number of major vessels (0-3) colored by flourosopy: ")
    thal = input("3 = normal; 6 = fixed defect; 7 = reversable defect: ")

    user_input = [age, sex, cp, trestbps, chol, fbs, restecg, thalach, exang, oldpeak, slope, ca, thal]
    user_input = np.array(user_input)
    user_input = user_input.reshape(1,-1).astype(float)
    user_input = scaler.transform(user_input.astype(float))
    prediction = clm.predict(user_input)

    if prediction[0] == 0:
        print('Alles Gut')
    else:
        print('Risiko für Heart Disease')