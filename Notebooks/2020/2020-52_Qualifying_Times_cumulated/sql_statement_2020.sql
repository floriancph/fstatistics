SELECT
	qualifying.q1,
	qualifying.q2,
	qualifying.q3,
	drivers.code,
	constructors."name",
	races."year",
	races.round,
	circuits."name"
FROM
	qualifying
	INNER JOIN drivers ON qualifying."driverId" = drivers."driverId"
	INNER JOIN races ON qualifying."raceId" = races."raceId"
	INNER JOIN circuits ON races."circuitId" = circuits."circuitId"
	INNER JOIN constructors ON qualifying."constructorId" = constructors."constructorId"
WHERE
	races.year = 2020