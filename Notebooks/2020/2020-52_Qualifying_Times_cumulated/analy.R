df_cum <- read.csv("/Users/flo/Desktop/fstatistics/2020-52_Qualifying_Times_cumulated/best_times_cumulated.csv")
df_total <- read.csv("/Users/flo/Desktop/fstatistics/2020-52_Qualifying_Times_cumulated/total_2019.csv")

df_total_add <- read.csv("/Users/flo/Desktop/fstatistics/2020-52_Qualifying_Times_cumulated/r_total_added.csv")

library(ggplot2)
ggplot(df_cum, aes(x=X)) + 
  geom_line(aes(y = df_cum$Alfa.Romeo), color = 'darkred', size = 1) + 
  geom_line(aes(y = df_cum$Mercedes), color = 'turquoise', size = 1) + 
  geom_line(aes(y = df_cum$Ferrari), color = 'red', size = 1) + 
  geom_line(aes(y = df_cum$Red.Bull), color = 'darkblue', size = 1) + 
  geom_line(aes(y = df_cum$AlphaTauri), color = 'black', size = 1) + 
  geom_line(aes(y = df_cum$McLaren), color = 'orange', size = 1) + 
  ggtitle("Positions: ") + 
  xlab("Lap") + 
  ylab("Position") +
  ylim(1000, 1500) +
  xlim(12, 16)


ggplot(data=df_total, aes(x=reorder(constructors, times), y=times)) +
  geom_bar(stat="identity") +
  ggtitle("Sum of the best Qualifying Times per Constructor in 2019") + 
  xlab("Constructor") + 
  ylab("Total seconds") + 
  coord_cartesian(ylim=c(1740,1820)) + 
  geom_text(aes(label=round(times, digits = 1)), vjust=1.6, color="white", size=3.5) +
  theme_minimal()

ggplot(data=df_total_add, aes(x=reorder(constructors, -diff_per_round), y=diff_per_round)) +
  geom_bar(stat="identity") +
  ggtitle("Average improvement on Best Laptimes 2019 vs. 2020") + 
  xlab("Constructor") + 
  ylab("Improvement in Seconds") + 
  coord_cartesian(ylim=c(0,3)) + 
  geom_text(aes(label=round(diff_per_round, digits = 3)), vjust=1.6, color="white", size=3.5) +
  theme_minimal()
