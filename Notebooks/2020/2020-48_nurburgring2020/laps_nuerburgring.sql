SELECT
	lap_times.lap,
	lap_times."position",
	lap_times."time",
	races."year",
	circuits. "name",
	drivers.code
FROM
	lap_times
	INNER JOIN races ON lap_times."raceId" = races. "raceId"
	INNER JOIN circuits ON races."circuitId" = circuits. "circuitId"
	INNER JOIN drivers ON lap_times."driverId" = drivers. "driverId"
WHERE
	circuits. "circuitId" = 20 AND races.year = 2020