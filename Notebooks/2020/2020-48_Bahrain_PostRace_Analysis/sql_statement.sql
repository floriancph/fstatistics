SELECT
	lap_times."time",
	lap_times."position",
	lap_times.lap,
	drivers.code,
	races."year",
	circuits."circuitId",
	circuits."name"
FROM
	lap_times
	INNER JOIN drivers ON lap_times."driverId" = drivers."driverId"
	INNER JOIN races ON lap_times."raceId" = races."raceId"
	INNER JOIN circuits ON races."circuitId" = circuits."circuitId"
WHERE
	circuits."circuitId" = 3 AND races.year = 2019