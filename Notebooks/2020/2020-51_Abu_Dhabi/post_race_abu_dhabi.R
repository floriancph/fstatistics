library(ggplot2)
library(gridExtra)
df <- read.csv(file = '/Users/flo/Desktop/fstatistics/2020-51_Abu_Dhabi/2020_round_17_laptimes.csv', sep = ',') # Read CSV

summary(df)

#####----- OVERALL BOXPLOTS -----#####
par(mfrow=c(1,2))
boxplot(df$secs, col="#69b3a2", main = "Laptime Distribution Sakhir 2020", ylab = "Laptime in Seconds")
boxplot(df$secs, outline = FALSE, col="#69b3a2", main = "Laptime Distribution Sakhir 2020\nwithout outliers", ylab = "Laptime in Seconds")
stripchart(df$secs, vertical = TRUE, data = df, method = "jitter", add = TRUE, pch = 20, col = 'grey')

# Delete Red Flag Values (>300)
# cond <- df$secs < 400
# df <- df[cond,]

# BOXPLOTS PER LAP
par(mfrow=c(1,1))
boxplot(df$secs ~ df$lap, outline = TRUE, col="#69b222", main = "Lap-by-Lap Time Distribution", ylab = "Laptime in Seconds", xlab = "Lap")
stripchart(df$secs ~ df$lap, vertical = TRUE, data = df, method = "jitter", add = TRUE, pch = 20, col = 'blue')


laptime_median_2020 <- aggregate(df$secs, list(df$lap), median)
names(laptime_median_2020)[1] <- "lap"
names(laptime_median_2020)[2] <- "median_laptime_2020"
plot_median_laptime_2020 <- ggplot(laptime_median_2020, aes(lap, median_laptime_2020)) + 
  geom_point() + 
  geom_smooth(method = "loess", color = 'darkgreen') + 
  # geom_smooth(method = lm, color = 'blue', linetype = "dashed", se = FALSE) +
  ggtitle("Median Laptimes Sakhir 2020") + 
  xlim(1,55) + 
  ylim(100,115) + 
  ylab("Median Laptime")

plot_median_laptime_2020

#####----- TEAM MATE COMPARISON -----#####

df_2020_pivot <- reshape(df, direction = "wide", idvar = "lap", timevar = "driver")

# ==> PLOT FUNCTIONS
plot_laptimes_regression <- function(df, lap, laptimes, color, name) {
  p <- ggplot(df, aes(lap, laptimes)) + 
    geom_point() + 
    geom_smooth(method = "loess", color = color) + 
    geom_line(aes(y = laptime_median_2020$median_laptime_2020), color = 'seagreen4', size = 0.5, linetype = "solid") +
    ggtitle(name) + 
    ylim(100,107) + 
    labs(x = "Lap",
         y = "Laptime in Seconds")
  return(p) 
}

plot_position_change <- function(df, lap, positions_d1, positions_d2, color_d1, color_d2, team_name) {
  p <- ggplot(df, aes(x=lap)) + 
    geom_line(aes(y = positions_d1), color = color_d1, size = 2) + 
    geom_line(aes(y = positions_d2), color= color_d2, size = 2) + 
    ggtitle("Positions: ", team_name) + 
    xlab("Lap") + 
    ylab("Position") + 
    ylim(1,20)
  return(p) 
}


# Mercedes
# ==> Positions
pos_merc <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.hamilton, df_2020_pivot$position.bottas, "turquoise", "pink", "George Russell (turquoise) & Sergio Perez (pink)")
pos_merc
plot_reg_ham <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.hamilton, "turquoise", "HAM")
plot_reg_bot <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.bottas, "turquoise", "BOT")
plot_reg_ver <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.max_verstappen, "darkblue", "VER")
plot_reg_alb <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.albon, "darkblue", "ALB")
grid.arrange(plot_reg_ham, plot_reg_bot, plot_reg_ver, plot_reg_alb, nrow = 2)


par(mfrow=c(2,2))
boxplot(df_2020_pivot$secs.hamilton, outline = FALSE, col="#69b222", main = "Laptimes Hamilton", ylab = "Laptime in Seconds", xlab = "Lap")

p1 <- ggplot(df_2020_pivot, aes(x='HAM' ,y=secs.hamilton)) + 
  geom_violin(trim=FALSE, fill='#A4A4A4', color="turquoise") + 
  stat_summary(fun=median, geom="point", size=2, color="red") + 
  geom_jitter(shape=16, position=position_jitter(0.2)) + 
  ylim(100,108)

p2 <- ggplot(df_2020_pivot, aes(x='BOT' ,y=secs.bottas)) + 
  geom_violin(trim=FALSE, fill='#A4A4A4', color="turquoise") + 
  stat_summary(fun=median, geom="point", size=2, color="red") + 
  geom_jitter(shape=16, position=position_jitter(0.2)) + 
  ylim(100,108)

p3 <- ggplot(df_2020_pivot, aes(x='VER' ,y=secs.max_verstappen)) + 
  geom_violin(trim=FALSE, fill='#A4A4A4', color="darkblue") + 
  stat_summary(fun=median, geom="point", size=2, color="red") + 
  geom_jitter(shape=16, position=position_jitter(0.2)) + 
  ylim(100,108)

p4 <- ggplot(df_2020_pivot, aes(x='ALB' ,y=secs.albon)) + 
  geom_violin(trim=FALSE, fill='#A4A4A4', color="darkblue") + 
  stat_summary(fun=median, geom="point", size=2, color="red") + 
  geom_jitter(shape=16, position=position_jitter(0.2)) + 
  ylim(100,108)

grid.arrange(p3, p2, p1, p4, nrow = 1)


cond1 <- df_2020_pivot$lap <= 10
cond2 <- df_2020_pivot$lap >= 11
par(bg = 'white')
par(mfrow=c(1,4))

ver_medium <- df_2020_pivot[cond1,]
ver_hards <- df_2020_pivot[cond2,]
boxplot(df_2020_pivot$secs.max_verstappen, outline = TRUE, col="#0A00D3", main = "Laptimes VER", ylab = "Laptime in Seconds", ylim=c(100.5,104.5))
stripchart(ver_medium$secs.max_verstappen, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = '#F4D02D')
stripchart(ver_hards$secs.max_verstappen, vertical = TRUE, method = "jitter", add = TRUE, pch = 17, col = 'black')
legend("top", 
       legend = c("Mediums", "Hards"), 
       col = c('#F4D02D', 'black'), 
       pch = c(20,17), 
       bty = "n", 
       pt.cex = 2, 
       cex = 1.2, 
       text.col = "black", 
       horiz = F , 
       inset = c(0.1, 0.1))

bot_medium <- df_2020_pivot[cond1,]
bot_hards <- df_2020_pivot[cond2,]
boxplot(df_2020_pivot$secs.bottas, outline = TRUE, col="#10E8C1", main = "Laptimes BOT", ylab = "Laptime in Seconds", ylim=c(100.5,104.5))
stripchart(bot_medium$secs.bottas, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = '#F4D02D')
stripchart(bot_hards$secs.bottas, vertical = TRUE, method = "jitter", add = TRUE, pch = 17, col = 'black')
legend("top", 
       legend = c("Mediums", "Hards"), 
       col = c('#F4D02D', 'black'), 
       pch = c(20,17), 
       bty = "n", 
       pt.cex = 2, 
       cex = 1.2, 
       text.col = "black", 
       horiz = F , 
       inset = c(0.1, 0.1))

ham_medium <- df_2020_pivot[cond1,]
ham_hards <- df_2020_pivot[cond2,]
boxplot(df_2020_pivot$secs.hamilton, outline = TRUE, col="#10E8C1", main = "Laptimes HAM", ylab = "Laptime in Seconds", ylim=c(100.5,104.5))
stripchart(ham_medium$secs.hamilton, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = '#F4D02D')
stripchart(ham_hards$secs.hamilton, vertical = TRUE, method = "jitter", add = TRUE, pch = 17, col = 'black')
legend("top", 
       legend = c("Mediums", "Hards"), 
       col = c('#F4D02D', 'black'), 
       pch = c(20,17), 
       bty = "n", 
       pt.cex = 2, 
       cex = 1.2, 
       text.col = "black", 
       horiz = F , 
       inset = c(0.1, 0.1))

alb_softs <- df_2020_pivot[cond1,]
alb_hards <- df_2020_pivot[cond2,]
boxplot(df_2020_pivot$secs.albon, outline = TRUE, col="#0A00D3", main = "Laptimes ALB", ylab = "Laptime in Seconds", ylim=c(100.5,104.5))
stripchart(alb_softs$secs.albon, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'red')
stripchart(alb_hards$secs.albon, vertical = TRUE, method = "jitter", add = TRUE, pch = 17, col = 'black')
legend("top", 
       legend = c("Softs", "Hards"), 
       col = c('red', 'black'), 
       pch = c(20,17), 
       bty = "n", 
       pt.cex = 2, 
       cex = 1.2, 
       text.col = "black", 
       horiz = F , 
       inset = c(0.1, 0.1))




# ==> Laptimes in context
boxplot(df_2020$secs ~ df_2020$lap, outline = FALSE, col="#69b3a2", main = "Lap-by-Lap Time Distribution Bahrain 2020 without Outliers\nMercedes", ylab = "Laptime in Seconds", xlab = "Lap")
stripchart(df_2020_pivot$secs.hamilton ~ df_2020_pivot$lap, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'turquoise')
stripchart(df_2020_pivot$secs.bottas ~ df_2020_pivot$lap, vertical = TRUE, method = "jitter", add = TRUE, pch = 17, col = 'black')
legend("topright", 
       legend = c("HAM", "BOT"), 
       col = c('turquoise', 
               'black'), 
       pch = c(20,17), 
       bty = "n", 
       pt.cex = 2, 
       cex = 1.2, 
       text.col = "black", 
       horiz = F , 
       inset = c(0.1, 0.1))

par(mfrow=c(1,2))
boxplot(df_2020_pivot$secs.russell, outline = FALSE, ylim = c(55, 60), main = "RUSSELL")
stripchart(df_2020_pivot$secs.russell, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'turquoise')
boxplot(df_2020_pivot$secs.bottas, outline = FALSE, ylim = c(55, 60), main = "BOTTAS")
stripchart(df_2020_pivot$secs.bottas, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'turquoise')

# Red Bull
# ==> Positions
pos_rb <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.max_verstappen, df_2020_pivot$position.albon, "darkblue", "blue", "Red Bull")
pos_rb
plot_reg_ver <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.max_verstappen, "darkblue", "VER")
plot_reg_alb <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.albon, "blue", "ALB")
grid.arrange(plot_reg_ver, plot_reg_alb, nrow = 1)


# McLaren
# ==> Positions
pos_mcl <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.norris, df_2020_pivot$position.sainz, "orange", "darkorange", "McLaren")
pos_mcl
plot_reg_nor <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.norris, "orange", "NOR")
plot_reg_sai <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.sainz, "darkorange", "SAI")
grid.arrange(plot_reg_nor, plot_reg_sai, nrow = 1)

# Renault
# ==> Positions
pos_ren <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.ricciardo, df_2020_pivot$position.ocon, "gold", "yellow", "Renault")
pos_ren
plot_reg_ric <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.ricciardo, "gold", "RIC")
plot_reg_oco <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.ocon, "gold", "OCO")
grid.arrange(plot_reg_ric, plot_reg_oco, nrow = 1)

# Ferrari
# ==> Positions
pos_fer <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.vettel, df_2020_pivot$position.leclerc, "darkred", "red", "Ferrari")
pos_fer
plot_reg_vet <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.vettel, "darkred", "VET")
plot_reg_lec <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.leclerc, "red", "LEC")
grid.arrange(plot_reg_vet, plot_reg_lec, nrow = 1)

# Racing Point
# ==> Positions
pos_rp <- plot_position_change(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$position.perez, df_2020_pivot$position.stroll, "pink", "purple", "Racing Point")
pos_rp
plot_reg_per <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.perez, "purple", "PER")
plot_reg_str <- plot_laptimes_regression(df_2020_pivot, df_2020_pivot$lap, df_2020_pivot$secs.stroll, "pink", "STR")
grid.arrange(plot_reg_per, plot_reg_str, nrow = 1)

down = 55
up = 62
par(mfrow=c(1,6))
boxplot(df_2020_pivot$secs.russell, outline = FALSE, ylim = c(down, up), main = "RUSSELL")
stripchart(df_2020_pivot$secs.russell, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, size = 10, col = 'turquoise')
boxplot(df_2020_pivot$secs.bottas, outline = FALSE, ylim = c(down, up), main = "BOTTAS")
stripchart(df_2020_pivot$secs.bottas, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'turquoise')
boxplot(df_2020_pivot$secs.perez, outline = FALSE, ylim = c(down, up), main = "PEREZ")
stripchart(df_2020_pivot$secs.perez, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'green')
boxplot(df_2020_pivot$secs.stroll, outline = FALSE, ylim = c(down, up), main = "STROLL")
stripchart(df_2020_pivot$secs.stroll, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'green')
boxplot(df_2020_pivot$secs.ocon, outline = FALSE, ylim = c(down, up), main = "OCON")
stripchart(df_2020_pivot$secs.ocon, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'gold')
boxplot(df_2020_pivot$secs.latifi, outline = FALSE, ylim = c(down, up), main = "LATIFI")
stripchart(df_2020_pivot$secs.latifi, vertical = TRUE, method = "jitter", add = TRUE, pch = 20, col = 'blue')



#####----- CONSISTENCY RANKING -----#####

sd_grouped <- aggregate(df_2020$secs, list(df_2020$driver), sd)
View(sd_grouped)
ggplot(data=sd_grouped, aes(x=reorder(Group.1, x), y=x)) +
  geom_bar(stat="identity") +
  ggtitle("Standard Deviation for each driver - 2020 Bahrain GP") + 
  xlab("Driver") + 
  ylab("Consistency based on Laptime (stat. standard deviation)")



