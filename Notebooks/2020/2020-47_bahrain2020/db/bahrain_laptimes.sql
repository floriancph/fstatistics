SELECT
	lap_times.lap,
	lap_times. "time",
	drivers.surname,
	drivers.code,
	lap_times. "raceId",
	races. "year",
	circuits. "name"
FROM
	lap_times
	INNER JOIN races ON lap_times. "raceId" = races. "raceId"
	INNER JOIN circuits ON races. "circuitId" = circuits. "circuitId"
	INNER JOIN drivers ON lap_times. "driverId" = drivers. "driverId"
WHERE
	circuits. "circuitId" = 3