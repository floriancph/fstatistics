SELECT
	races. "year",
	qualifying.q1,
	qualifying.q2,
	qualifying.q3,
	circuits. "name",
	drivers.surname,
	drivers.code
FROM
	qualifying
	INNER JOIN races ON qualifying. "raceId" = races. "raceId"
	INNER JOIN circuits ON races. "circuitId" = circuits. "circuitId"
	INNER JOIN drivers ON qualifying. "driverId" = drivers. "driverId"
WHERE
	circuits. "circuitId" = 3