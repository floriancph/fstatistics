library(ggplot2)
library(mosaic)
df <- read.csv(file = '/Users/flo/Desktop/fstatistics/bahrain2020/Qualifying\ Prediction/bahrain_qualifying.csv', sep = ',') # Read CSV
summary(df)

# Boxplot aller Q3 Zeiten über die Jahre
boxplot(df$q3_sec ~ df$year, main="Formula 1 Q3-Times in Bahrain since 2006", xlab="Year", ylab="Laptime in Seconds")

# Exclude 2010-times
cond <- df$year != 2010
df <- df[cond,]

# Boxplot aller Q3 Zeiten über die Jahre
boxplot(df$q3_sec ~ df$year, main="Formula 1 Q3-Times in Bahrain since 2006 (without 2010 GP)", xlab="Year", ylab="Laptime in Seconds")

# Durchschnitt & Median über alle 
years_grouped <- aggregate(df[9], list(df$year), mean)
names(years_grouped)[1] <- "year"
names(years_grouped)[2] <- "q3_mean"
median <- aggregate(df[9], list(df$year), median)
years_grouped$q3_median <- median$q3_sec
ggplot(years_grouped, aes(x=year)) + geom_line(aes(y = q3_mean), color = "darkred") + geom_line(aes(y = q3_median), color="steelblue", linetype="twodash")

# Verteilung der Means (ohne 2010)
boxplot(years_grouped$q3_mean, outline=FALSE, main="Formula 1 Q3-Times (mean) in Bahrain since 2006 (without 2010)", ylab="Seconds")

# Prediction der Mean Werte
Ergebnis_all <- lm(q3_mean ~ year, data = years_grouped)
plotModel(Ergebnis_all)
summary(Ergebnis_all)
new <- data.frame(year = c(2020, 2021))
pred.point <- predict(Ergebnis_all, newdata = new)
pred.point

# Prediction der Mean Werte von den letzten 4 Jahren
last_years <- years_grouped[-c(1:8),]
View(last_years)
Ergebnis_last <- lm(q3_mean ~ year, data = last_years) # Model
summary(Ergebnis_last)
plotModel(Ergebnis_last)

# Prediction Mean-Werte für Jahr 2020 und 2021
new <- data.frame(year = c(2020, 2021))
pred.point <- predict(Ergebnis_last, newdata = new)
pred.point
